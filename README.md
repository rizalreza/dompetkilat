
## How To Use 
#### Requirement
- Docker

**Clone and Running**

* Clone from repository
```		
https://gitlab.com/rizalreza/dompetkilat.git
```

* Move to project directory
```
cd dompetkilat
```
* Copy .env from .env.example
```
cp .env.example .env
```

* Build docker
```
docker-compose up --build
```

* Please import postman collection attached for api documentation


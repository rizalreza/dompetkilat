package seed

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/rizalreza/dompetkilat/api/models"
)

var users = []models.User{
	{
		Username: "rizal",
		Email:    "rizal@gmail.com",
		Password: "password",
	},
	{
		Username: "reza",
		Email:    "reza@gmail.com",
		Password: "password",
	},
}

var financings = []models.Financing{
	{
		Name:  "Invoice Financing",
		Count: 35,
	},
	{
		Name:  "OSF Financing",
		Count: 25,
	},
	{
		Name:  "SBN",
		Count: 10,
	},
	{
		Name:  "Reksadana",
		Count: 20,
	},
	{
		Name:  "Conventional Invoice",
		Count: 20,
		Sub:   "invoice",
	},
	{
		Name:  "Productive Invoice",
		Count: 15,
		Sub:   "invoice",
	},
	{
		Name:  "Conventional OSF",
		Count: 15,
		Sub:   "osf",
	},
	{
		Name:  "Productive OSF",
		Count: 10,
		Sub:   "osf",
	},
}

var ConventionalOsf = []models.SubFinancing{
	{
		Name:        "PT YJK",
		Amount:      1000000,
		Tenor:       120,
		Grade:       "B",
		Rate:        16,
		FinancingId: 7,
	},
	{
		Name:        "PT KKY",
		Amount:      4000000,
		Tenor:       120,
		Grade:       "B+",
		Rate:        14,
		FinancingId: 7,
	},
}

var reksadana = []models.SubFinancing{
	{
		Name:        "INB",
		Amount:      20000000,
		Return:      -1,
		FinancingId: 4,
	},
	{
		Name:        "PT TELKOM",
		Amount:      10000000,
		Return:      1,
		FinancingId: 4,
	},
}

var sbn = []models.SubFinancing{
	{
		Name:        "SBRXXX",
		Amount:      1000000,
		Tenor:       120,
		Rate:        7,
		Type:        "SBR",
		FinancingId: 3,
	},
	{
		Name:        "SBRYYY",
		Amount:      2000000,
		Tenor:       120,
		Rate:        8,
		Type:        "SBR",
		FinancingId: 3,
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.User{}, &models.Financing{}, &models.SubFinancing{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.User{}, &models.Financing{}, &models.SubFinancing{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users data: %v", err)
		}
	}
	for i, _ := range financings {
		err = db.Debug().Model(&models.Financing{}).Create(&financings[i]).Error
		if err != nil {
			log.Fatalf("cannot seed financing data: %v", err)
		}
	}
	for i, _ := range ConventionalOsf {
		err = db.Debug().Model(&models.SubFinancing{}).Create(&ConventionalOsf[i]).Error
		if err != nil {
			log.Fatalf("cannot seed ConventionalOsf data: %v", err)
		}
	}
	for i, _ := range reksadana {
		err = db.Debug().Model(&models.SubFinancing{}).Create(&reksadana[i]).Error
		if err != nil {
			log.Fatalf("cannot seed reksadana data: %v", err)
		}
	}
	for i, _ := range sbn {
		err = db.Debug().Model(&models.SubFinancing{}).Create(&sbn[i]).Error
		if err != nil {
			log.Fatalf("cannot seed sbn data: %v", err)
		}
	}
}

package controllers

import (
	"net/http"
	"strconv"

	"gitlab.com/rizalreza/dompetkilat/api/models"
	"gitlab.com/rizalreza/dompetkilat/api/responses"
	"gitlab.com/rizalreza/dompetkilat/api/utils/formaterror"
)

func (server *Server) CreateFinancing(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	sub := r.FormValue("sub")
	count, err := strconv.Atoi(r.FormValue("count"))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	model := models.Financing{}
	model.Name = name
	model.Count = count
	model.Sub = sub
	err = model.ValidateFinancing("create")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	dataCreated, err := model.CreateFinancing(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	responses.JSON(w, http.StatusOK, map[string]interface{}{"message": "Create data successfully", "data": dataCreated})
}

func (server *Server) GetAllFinancing(w http.ResponseWriter, r *http.Request) {

	model := models.Financing{}

	data, err := model.GetAllFinancing(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, map[string]interface{}{"message": "Fetch data successfully", "data": data})
}

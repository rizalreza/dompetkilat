package controllers

import "gitlab.com/rizalreza/dompetkilat/api/middlewares"

func (s *Server) initializeRoutes() {
	s.Router.HandleFunc("/", middlewares.SetMiddlewareJSON(s.Home)).Methods("GET")

	// Login Route
	s.Router.HandleFunc("/login", middlewares.SetMiddlewareJSON(s.Login)).Methods("POST")
	s.Router.HandleFunc("/register", middlewares.SetMiddlewareJSON(s.Register)).Methods("POST")

	// Financing Route
	s.Router.HandleFunc("/financing", middlewares.SetMiddlewareJSON(s.CreateFinancing)).Methods("POST")
	s.Router.HandleFunc("/financing", middlewares.SetMiddlewareJSON(s.GetAllFinancing)).Methods("GET")
	s.Router.HandleFunc("/convetional-osf", middlewares.SetMiddlewareJSON(s.CreateConventionalOsf)).Methods("POST")
	s.Router.HandleFunc("/convetional-osf", middlewares.SetMiddlewareJSON(s.GetAllConventionalOsf)).Methods("GET")
	s.Router.HandleFunc("/convetional-invoice", middlewares.SetMiddlewareJSON(s.CreateConventionalInvoice)).Methods("POST")
	s.Router.HandleFunc("/convetional-invoice", middlewares.SetMiddlewareJSON(s.GetAllConventionalInvoice)).Methods("GET")
	s.Router.HandleFunc("/productive-invoice", middlewares.SetMiddlewareJSON(s.CreateProductiveInvoice)).Methods("POST")
	s.Router.HandleFunc("/productive-invoice", middlewares.SetMiddlewareJSON(s.GetAllProductiveInvoice)).Methods("GET")
	s.Router.HandleFunc("/reksadana", middlewares.SetMiddlewareJSON(s.CreateReksadana)).Methods("POST")
	s.Router.HandleFunc("/reksadana", middlewares.SetMiddlewareJSON(s.GetAllReksadana)).Methods("GET")
	s.Router.HandleFunc("/sbn", middlewares.SetMiddlewareJSON(s.CreateSbn)).Methods("POST")
	s.Router.HandleFunc("/sbn", middlewares.SetMiddlewareJSON(s.GetAllSbn)).Methods("GET")

}

package controllers

import (
	"net/http"

	"gitlab.com/rizalreza/dompetkilat/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Hello world")
}

package controllers

import (
	"net/http"

	"gitlab.com/rizalreza/dompetkilat/api/models"
	"gitlab.com/rizalreza/dompetkilat/api/responses"
	"gitlab.com/rizalreza/dompetkilat/api/utils/formaterror"
)

func (server *Server) CreateConventionalInvoice(ResponseWriter http.ResponseWriter, Request *http.Request) {

	model, err := models.HandleRequest(Request, ResponseWriter, models.ConventionalInvoice)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	err = model.ValidateSubFinancing(models.ConventionalInvoice)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	dataCreated, err := model.CreateSubFinancing(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, formattedError)
		return
	}
	data := map[string]interface{}{"name": dataCreated.Name, "grade": dataCreated.Grade, "amount": dataCreated.Amount, "tenor": dataCreated.Tenor, "rate": dataCreated.Rate}
	responses.JSON(ResponseWriter, http.StatusCreated, map[string]interface{}{"message": "Create data successfully", "data": data})

}

func (server *Server) CreateProductiveInvoice(ResponseWriter http.ResponseWriter, Request *http.Request) {
	model, err := models.HandleRequest(Request, ResponseWriter, models.ProductiveInvoice)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	err = model.ValidateSubFinancing(models.ProductiveInvoice)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, formattedError)
		return
	}

	dataCreated, err := model.CreateSubFinancing(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, formattedError)
		return
	}

	data := map[string]interface{}{"name": dataCreated.Name, "grade": dataCreated.Grade, "amount": dataCreated.Amount, "tenor": dataCreated.Tenor, "rate": dataCreated.Rate}
	responses.JSON(ResponseWriter, http.StatusCreated, map[string]interface{}{"message": "Create data successfully", "data": data})
}

func (server *Server) CreateConventionalOsf(ResponseWriter http.ResponseWriter, Request *http.Request) {

	model, err := models.HandleRequest(Request, ResponseWriter, models.ConventionalOsf)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	err = model.ValidateSubFinancing(models.ConventionalOsf)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	dataCreated, err := model.CreateSubFinancing(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, formattedError)
		return
	}
	data := map[string]interface{}{"name": dataCreated.Name, "grade": dataCreated.Grade, "amount": dataCreated.Amount, "tenor": dataCreated.Tenor, "rate": dataCreated.Rate}
	responses.JSON(ResponseWriter, http.StatusCreated, map[string]interface{}{"message": "Create data successfully", "data": data})
}

func (server *Server) CreateReksadana(ResponseWriter http.ResponseWriter, Request *http.Request) {

	model, err := models.HandleRequestReksadana(Request, ResponseWriter, models.Reksadana)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	err = model.ValidateSubFinancing(models.Reksadana)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	dataCreated, err := model.CreateSubFinancing(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, formattedError)
		return
	}
	data := map[string]interface{}{"name": dataCreated.Name, "amount": dataCreated.Amount, "return": dataCreated.Return}
	responses.JSON(ResponseWriter, http.StatusCreated, map[string]interface{}{"message": "Create data successfully", "data": data})

}

func (server *Server) CreateSbn(ResponseWriter http.ResponseWriter, Request *http.Request) {

	model, err := models.HandleRequestSbn(Request, ResponseWriter, models.SBN)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	err = model.ValidateSubFinancing(models.SBN)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusUnprocessableEntity, err)
		return
	}

	dataCreated, err := model.CreateSubFinancing(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, formattedError)
		return
	}
	data := map[string]interface{}{"name": dataCreated.Name, "amount": dataCreated.Amount, "tenor": dataCreated.Tenor, "rate": dataCreated.Rate, "type": dataCreated.Type}
	responses.JSON(ResponseWriter, http.StatusCreated, map[string]interface{}{"message": "Create data successfully", "data": data})

}

func (server *Server) GetAllConventionalInvoice(ResponseWriter http.ResponseWriter, r *http.Request) {

	model := models.SubFinancing{}

	data, err := model.GetSubFinancing(server.DB, models.ConventionalInvoice)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(ResponseWriter, http.StatusOK, map[string]interface{}{"message": "Fetch data successfully", "data": data})
}

func (server *Server) GetAllProductiveInvoice(ResponseWriter http.ResponseWriter, r *http.Request) {

	model := models.SubFinancing{}

	data, err := model.GetSubFinancing(server.DB, models.ProductiveInvoice)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(ResponseWriter, http.StatusOK, map[string]interface{}{"message": "Fetch data successfully", "data": data})
}

func (server *Server) GetAllConventionalOsf(ResponseWriter http.ResponseWriter, r *http.Request) {

	model := models.SubFinancing{}

	data, err := model.GetSubFinancing(server.DB, models.ConventionalOsf)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(ResponseWriter, http.StatusOK, map[string]interface{}{"message": "Fetch data successfully", "data": data})
}

func (server *Server) GetAllReksadana(ResponseWriter http.ResponseWriter, r *http.Request) {

	model := models.SubFinancing{}

	data, err := model.GetSubFinancing(server.DB, models.Reksadana)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(ResponseWriter, http.StatusOK, map[string]interface{}{"message": "Fetch data successfully", "data": data})
}
func (server *Server) GetAllSbn(ResponseWriter http.ResponseWriter, r *http.Request) {

	model := models.SubFinancing{}

	data, err := model.GetSubFinancing(server.DB, models.SBN)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(ResponseWriter, http.StatusOK, map[string]interface{}{"message": "Fetch data successfully", "data": data})
}

package models

import (
	"errors"
	"html"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/rizalreza/dompetkilat/api/responses"
)

type SubFinancing struct {
	ID          uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Name        string    `gorm:"size:100;" json:"name"`
	Amount      int       `gorm:"size:11;" json:"amount"`
	Tenor       int       `gorm:"size:11;" json:"tenor"`
	Grade       string    `gorm:"size:100;" json:"grade"`
	Rate        int       `gorm:"size:11;" json:"rate"`
	Return      int       `gorm:"size:11;" json:"return"`
	Type        string    `gorm:"size:100;" json:"type"`
	FinancingId uint32    `gorm:"not null;" json:"financing_id"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

var SBN uint32 = 3
var Reksadana uint32 = 4
var ConventionalInvoice uint32 = 5
var ProductiveInvoice uint32 = 6
var ConventionalOsf uint32 = 7

func (sub *SubFinancing) PrepareSubFinancing() {
	sub.ID = 0
	sub.Name = html.EscapeString(strings.TrimSpace(sub.Name))
	sub.Amount = 0
	sub.Tenor = 0
	sub.Grade = html.EscapeString(strings.TrimSpace(sub.Grade))
	sub.Rate = 0
	sub.Return = 0
	sub.FinancingId = 0
	sub.Type = html.EscapeString(strings.TrimSpace(sub.Type))
	sub.CreatedAt = time.Now()
	sub.UpdatedAt = time.Now()
}

func (sub *SubFinancing) ValidateSubFinancing(action uint32) error {
	// return strings.ToLower(action)
	switch action {
	case ConventionalInvoice, ProductiveInvoice, ConventionalOsf:
		if sub.Name == "" {
			return errors.New("Required Name")
		}
		if sub.Amount == 0 {
			return errors.New("Required Amount")
		}
		if sub.Tenor == 0 {
			return errors.New("Required Tenor")
		}
		if sub.Grade == "" {
			return errors.New("Required Grade")
		}
		if sub.Rate == 0 {
			return errors.New("Required Rate")
		}
		return nil
	case Reksadana:
		if sub.Name == "" {
			return errors.New("Required Name")
		}
		if sub.Amount == 0 {
			return errors.New("Required Amount")
		}
		if sub.Return == 0 {
			return errors.New("Required Return")
		}
		return nil
	case SBN:
		if sub.Name == "" {
			return errors.New("Required Name")
		}
		if sub.Amount == 0 {
			return errors.New("Required Amount")
		}
		if sub.Tenor == 0 {
			return errors.New("Required Tenor")
		}
		if sub.Rate == 0 {
			return errors.New("Required Rate")
		}
		if sub.Type == "" {
			return errors.New("Required Type")
		}
		return nil
	default:
		if sub.Name == "" {
			return errors.New("Required Name")
		}
		if sub.Amount == 0 {
			return errors.New("Required Amount")
		}
		if sub.Tenor == 0 {
			return errors.New("Required Tenor")
		}
		if sub.Grade == "" {
			return errors.New("Required Grade")
		}
		if sub.Rate == 0 {
			return errors.New("Required Rate")
		}
		return nil
	}
}

func (sub *SubFinancing) CreateSubFinancing(db *gorm.DB) (*SubFinancing, error) {
	var err error
	err = db.Debug().Create(&sub).Error
	if err != nil {
		return &SubFinancing{}, err
	}
	return sub, nil
}

func (sub *SubFinancing) GetSubFinancing(db *gorm.DB, id uint32) ([]interface{}, error) {
	var err error
	data := []SubFinancing{}
	err = db.Debug().Model(&Financing{}).Where("financing_id = ?", id).Limit(100).Find(&data).Error
	if err != nil {
		return nil, err
	}
	result, err := GetResultByType(data, id)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func GetResultByType(data []SubFinancing, id uint32) ([]interface{}, error) {
	var result []interface{}
	for i, _ := range data {
		switch id {
		case ConventionalInvoice, ProductiveInvoice, ConventionalOsf:
			obj := map[string]interface{}{"name": data[i].Name, "grade": data[i].Grade, "amount": data[i].Amount, "tenor": data[i].Tenor, "rate": data[i].Rate}
			result = append(result, obj)
		case Reksadana:
			obj := map[string]interface{}{"name": data[i].Name, "amount": data[i].Amount, "retturn": data[i].Return}
			result = append(result, obj)
		case SBN:
			obj := map[string]interface{}{"name": data[i].Name, "amount": data[i].Amount, "tenor": data[i].Tenor, "rate": data[i].Rate, "type": data[i].Type}
			result = append(result, obj)

		default:
			return nil, errors.New("Financing type required")
		}

	}
	return result, nil
}

func HandleRequest(Request *http.Request, ResponseWriter http.ResponseWriter, financing_id uint32) (*SubFinancing, error) {
	name := Request.FormValue("name")
	grade := Request.FormValue("grade")
	amount := Request.FormValue("amount")
	tenor := Request.FormValue("tenor")
	rate := Request.FormValue("rate")
	if (amount) == "" {
		return nil, errors.New("Required Amount")
	}
	if (tenor) == "" {
		return nil, errors.New("Required Tenor")
	}
	if (rate) == "" {
		return nil, errors.New("Required Rate")
	}
	amountConverted, err := strconv.Atoi(amount)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}
	tenorConverted, err := strconv.Atoi(tenor)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}
	rateConverted, err := strconv.Atoi(rate)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}

	model := SubFinancing{}
	model.Name = name
	model.Grade = grade
	model.Amount = amountConverted
	model.Tenor = tenorConverted
	model.Rate = rateConverted
	model.FinancingId = financing_id

	return &model, nil

}

func HandleRequestReksadana(Request *http.Request, ResponseWriter http.ResponseWriter, financing_id uint32) (*SubFinancing, error) {
	name := Request.FormValue("name")
	amount := Request.FormValue("amount")
	returns := Request.FormValue("return")
	if (amount) == "" {
		return nil, errors.New("Required Amount")
	}
	if (returns) == "" {
		return nil, errors.New("Required Return")
	}
	amountConverted, err := strconv.Atoi(amount)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}
	returnConverted, err := strconv.Atoi(returns)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}

	model := SubFinancing{}
	model.Name = name
	model.Amount = amountConverted
	model.Return = returnConverted
	model.FinancingId = financing_id

	return &model, nil

}

func HandleRequestSbn(Request *http.Request, ResponseWriter http.ResponseWriter, financing_id uint32) (*SubFinancing, error) {
	name := Request.FormValue("name")
	amount := Request.FormValue("amount")
	tenor := Request.FormValue("tenor")
	rate := Request.FormValue("rate")
	finance_type := Request.FormValue("type")
	if (amount) == "" {
		return nil, errors.New("Required Amount")
	}
	if (tenor) == "" {
		return nil, errors.New("Required Tenor")
	}
	if (rate) == "" {
		return nil, errors.New("Required Rate")
	}
	amountConverted, err := strconv.Atoi(amount)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}
	tenorConverted, err := strconv.Atoi(tenor)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}
	rateConverted, err := strconv.Atoi(rate)
	if err != nil {
		responses.ERROR(ResponseWriter, http.StatusBadRequest, err)
	}

	model := SubFinancing{}
	model.Name = name
	model.Amount = amountConverted
	model.Tenor = tenorConverted
	model.Rate = rateConverted
	model.Type = finance_type
	model.FinancingId = financing_id

	return &model, nil

}

package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Financing struct {
	ID        uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Name      string    `gorm:"size:100;not null;unique" json:"name"`
	Count     int       `gorm:"size:11;" json:"count"`
	Sub       string    `gorm:"size:100;" json:"sub"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (financing *Financing) PrepareFinancing() {
	financing.ID = 0
	financing.Name = html.EscapeString(strings.TrimSpace(financing.Name))
	financing.Count = 0
	financing.Sub = html.EscapeString(strings.TrimSpace(financing.Sub))
	financing.CreatedAt = time.Now()
	financing.UpdatedAt = time.Now()
}

func (financing *Financing) ValidateFinancing(action string) error {
	switch strings.ToLower(action) {
	case "create":
		if financing.Name == "" {
			return errors.New("Required Name")
		}
		if financing.Count == 0 {
			return errors.New("Required Count")
		}
		if financing.Sub == "" {
			return errors.New("Required Sub")
		}
		return nil
	default:
		if financing.Name == "" {
			return errors.New("Required Name")
		}
		if financing.Count == 0 {
			return errors.New("Required Count")
		}
		if financing.Sub == "" {
			return errors.New("Required Sub")
		}
		return nil
	}
}

func (financing *Financing) CreateFinancing(db *gorm.DB) (*Financing, error) {
	var err error
	err = db.Debug().Create(&financing).Error
	if err != nil {
		return &Financing{}, err
	}
	return financing, nil
}

func (financing *Financing) GetAllFinancing(db *gorm.DB) (*[]Financing, error) {
	var err error
	data := []Financing{}
	err = db.Debug().Model(&Financing{}).Limit(100).Find(&data).Error
	if err != nil {
		return &[]Financing{}, err
	}
	return &data, nil
}

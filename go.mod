module gitlab.com/rizalreza/dompetkilat

go 1.17

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa
)

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/joho/godotenv v1.4.0
	gopkg.in/go-playground/assert.v1 v1.2.1
)
